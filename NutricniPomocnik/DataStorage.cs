using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using System.IO;
using System.Collections.ObjectModel;

namespace NutricniPomocnik
{
    //class for operation with data
    public class DataStorage
    {
        private static SQLiteConnection db;
        public static List<Meal> mealList;
        public static List<EatenMealItem> eatenMeallist;
        public static DataPerDay dataPerDay;

        //saved meals
        [Table("Items")]
        public class MealItem
        {
            [PrimaryKey, Column("_id")]
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Bilkoviny { get; set; }
            public int Sacharidy { get; set; }
        }

        //this can be use in the future for some operations with a single eaten meals
        public class EatenMealItem
        {
            [PrimaryKey, Column("_id")]
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int Bilkoviny { get; set; }
            public int Sacharidy { get; set; }
        }

        //data about meals per day
        public class DataPerDay
        {
            [PrimaryKey, Column("_id")]
            public Guid Id { get; set; }
            public DateTime Date { get; set; }
            public int Bilkoviny { get; set; }
            public int Sacharidy { get; set; }
            public int BilkovinyCelkem { get; set; }
            public int SacharidyCelkem { get; set; }
        }

        public DataStorage()
        {
            string dbPath = Path.Combine(
                System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                "meals.db3");
            Console.WriteLine(dbPath);
            db = new SQLiteConnection(dbPath);
            db.CreateTable<MealItem>();
            db.CreateTable<EatenMealItem>();
            db.CreateTable<DataPerDay>();
            eatenMeallist = GetEatenMeals();
            mealList = GetItems();
            GetDataPerDay();
        }
        
        //add meal to storage
        public static void AddItem(Meal meal)
        {
            mealList.Add(meal);
            var item = new MealItem
            {
                Name = meal.Name,
                Bilkoviny = meal.Bilkoviny,
                Sacharidy = meal.Sacharidy,
                Id = meal._id
            };
            db.Insert(item);
        }

        //remove meal from storage
        public static void DeleteItem(Guid _id)
        {
            int mindex = mealList.IndexOf(mealList.First(m => m._id == _id));
            mealList.RemoveAt(mindex);
            db.Delete<MealItem>(_id);
        }

        //get list of meals
        private List<Meal> GetItems()
        {
            List<Meal> items = new List<Meal>();
            var table = db.Table<MealItem>();
            foreach (var m in table)
            {
                items.Add(new Meal { Name = m.Name, Bilkoviny = m.Bilkoviny, Sacharidy = m.Sacharidy, _id = m.Id });
            }
            return items;
        }

        //this method can be use in the future for some operations with a single eaten meals
        public static void EditItem(Guid _id, Meal m)
        {
            DeleteItem(_id);
            m._id = _id;
            AddItem(m);
        }

        //this method can be use in the future for some operations with a single eaten meals
        public static void AddEatenItem(Meal m)
        {
            EatenMealItem em = new EatenMealItem();
            em.Name = m.Name;
            em.Sacharidy = m.Sacharidy;
            em.Bilkoviny = m.Bilkoviny;
            em.Id = Guid.NewGuid();
            eatenMeallist.Add(em);
            db.Insert(em);
        }

        //this method can be use in the future for some operations with a single eaten meals
        public static void DeleteAllEatenItems()
        {
            foreach (EatenMealItem m in eatenMeallist)
            {
                db.Delete<EatenMealItem>(m.Id);
            }
            eatenMeallist.RemoveAll(m => m.Id != null);
        }

        //this method can be use in the future for some operations with a single eaten meals
        public List<EatenMealItem> GetEatenMeals()
        {
            var list = new List<EatenMealItem>();
            var table = db.Table<EatenMealItem>();
            foreach (var m in table)
            {
                list.Add(new EatenMealItem { Name = m.Name, Bilkoviny = m.Bilkoviny, Sacharidy = m.Sacharidy, Id = m.Id });
            }
            return list;
        } 

        //get data per day
        public static void GetDataPerDay()
        {
            dataPerDay = null;
            var table = db.Table<DataPerDay>();
            foreach (var d in table)
            {
                dataPerDay = new DataPerDay { Date = d.Date, Id = d.Id, Bilkoviny = d.Bilkoviny, Sacharidy = d.Sacharidy, BilkovinyCelkem = d.BilkovinyCelkem, SacharidyCelkem = d.SacharidyCelkem };
            }
            if((dataPerDay == null)||(dataPerDay.Date.Date != DateTime.Now.Date))
            {
                if(dataPerDay == null)
                {
                    dataPerDay = new DataPerDay { Date = DateTime.Now, Sacharidy = 0, Bilkoviny = 0, Id = Guid.NewGuid() };
                }
                else
                {
                    dataPerDay = new DataPerDay { Date = DateTime.Now, Sacharidy = 0, Bilkoviny = 0, Id = Guid.NewGuid(), BilkovinyCelkem = dataPerDay.BilkovinyCelkem, SacharidyCelkem = dataPerDay.SacharidyCelkem };
                }
                db.Insert(dataPerDay);
            }
        }

        //update data per day
        public static void UpdateDataPerDay(int bilkovinyPlus, int sacharidyPlus)
        {
            GetDataPerDay();
            var data = new DataPerDay { Date = dataPerDay.Date, Bilkoviny = dataPerDay.Bilkoviny, Id = dataPerDay.Id, Sacharidy = dataPerDay.Sacharidy, SacharidyCelkem = dataPerDay.SacharidyCelkem, BilkovinyCelkem = dataPerDay.BilkovinyCelkem };
            data.Bilkoviny += bilkovinyPlus;
            data.Sacharidy += sacharidyPlus;
            dataPerDay = data;
            db.Delete<DataPerDay>(dataPerDay.Id);
            db.Insert(dataPerDay);
        }

        //set data per day
        public static void SetDataPerDay(int bilkovinyPlus, int sacharidyPlus)
        {
            GetDataPerDay();
            var data = new DataPerDay { Date = dataPerDay.Date, Bilkoviny = dataPerDay.Bilkoviny, Id = dataPerDay.Id, Sacharidy = dataPerDay.Sacharidy};
            data.BilkovinyCelkem = bilkovinyPlus;
            data.SacharidyCelkem = sacharidyPlus;
            dataPerDay = data;
            db.Delete<DataPerDay>(dataPerDay.Id);
            db.Insert(dataPerDay);
        }

        //clear data per day
        public static void ClearDataPerDay()
        {
            GetDataPerDay();
            var data = new DataPerDay { Date = dataPerDay.Date, Bilkoviny = dataPerDay.Bilkoviny, Id = dataPerDay.Id, Sacharidy = dataPerDay.Sacharidy, SacharidyCelkem = dataPerDay.SacharidyCelkem, BilkovinyCelkem = dataPerDay.BilkovinyCelkem };
            data.Bilkoviny = 0;
            data.Sacharidy = 0;
            dataPerDay = data;
            db.Delete<DataPerDay>(dataPerDay.Id);
            db.Insert(dataPerDay);
        }
    }
}