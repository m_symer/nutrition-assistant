using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.ObjectModel;

namespace NutricniPomocnik
{
    //for correct list view
    public class MealsAdapter : BaseAdapter<Meal>
    {
        List<Meal> items;
        Activity context;

        public MealsAdapter(Activity context, List<Meal> items)
            : base()
        {
            this.context = context;
            this.items = items;
        }

        public override Meal this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            NotifyDataSetChanged();
            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.MealListItem, null);
            view.FindViewById<TextView>(Resource.Id.textViewNazev).Text = item.Name;
            view.FindViewById<TextView>(Resource.Id.textViewBilkoviny).Text = context.Resources.GetString(Resource.String.BilkovinyNadpis) + ": " + item.Bilkoviny.ToString();
            view.FindViewById<TextView>(Resource.Id.textViewSacharidy).Text = context.Resources.GetString(Resource.String.SacharidyNadpis) + ": " + item.Sacharidy.ToString();

            return view;
        }
    }
}