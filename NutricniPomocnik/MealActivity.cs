using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    [Activity(Label = "Meal")]
    public class MealActivity : Activity
    {
        int mealToEdit;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Meal);

            Button addMealButton = FindViewById<Button>(Resource.Id.buttonPridat);
            addMealButton.Click += AddMeal;

            mealToEdit = Intent.GetIntExtra("MealToEdit",-1);

            if(mealToEdit >= 0)
            {
                FindViewById<Button>(Resource.Id.buttonPridat).Text = this.Resources.GetString(Resource.String.UlozitButton);
                FindViewById<EditText>(Resource.Id.editTextJidloNazev).Text = DataStorage.mealList[mealToEdit].Name;
                FindViewById<EditText>(Resource.Id.editTextJidloBilkoviny).Text = DataStorage.mealList[mealToEdit].Bilkoviny.ToString();
                FindViewById<EditText>(Resource.Id.editTextJIdloSacharidy).Text = DataStorage.mealList[mealToEdit].Sacharidy.ToString();
            }
        }

        protected void AddMeal(object sender, EventArgs e)
        {
            EditText pname = FindViewById<EditText>(Resource.Id.editTextJidloNazev);
            EditText pbilkoviny = FindViewById<EditText>(Resource.Id.editTextJidloBilkoviny);
            EditText psacharidy = FindViewById<EditText>(Resource.Id.editTextJIdloSacharidy);
            if (mealToEdit < 0) {
                DataStorage.AddItem(new Meal { Name = pname.Text, Bilkoviny = Int32.Parse(pbilkoviny.Text), Sacharidy = Int32.Parse(psacharidy.Text), _id = Guid.NewGuid()});
            }else
            {
                DataStorage.EditItem(DataStorage.mealList[mealToEdit]._id, new Meal { Name = pname.Text, Bilkoviny = Int32.Parse(pbilkoviny.Text), Sacharidy = Int32.Parse(psacharidy.Text) });
            }
            Finish();
        }
    }
}