using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    [Activity(Label = "Settings")]
    public class SettingsActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Settings);
            Button vynulovatButton = FindViewById<Button>(Resource.Id.buttonRestartDne);
            Button ulozitButton = FindViewById<Button>(Resource.Id.buttonUlozitNastaveni);

            vynulovatButton.Click += VynulovatDen;
            ulozitButton.Click += SaveClick;

            FindViewById<EditText>(Resource.Id.editTextBilkovinyCelkem).Text = DataStorage.dataPerDay.BilkovinyCelkem.ToString();
            FindViewById<EditText>(Resource.Id.editTextSacharidyCelkem).Text = DataStorage.dataPerDay.SacharidyCelkem.ToString();
        }

        protected void SaveClick(object sender, EventArgs e)
        {
            var bilkoviny = FindViewById<EditText>(Resource.Id.editTextBilkovinyCelkem);
            var sacharidy = FindViewById<EditText>(Resource.Id.editTextSacharidyCelkem);
            DataStorage.SetDataPerDay(Int32.Parse(bilkoviny.Text), Int32.Parse(sacharidy.Text));
            Finish();
        }
        protected void VynulovatDen(object sender, EventArgs e)
        {
            var callDialog = new AlertDialog.Builder(this);
            callDialog.SetMessage(Resources.GetString(Resource.String.InfoOVynulovani));
            callDialog.SetPositiveButton("Ok", delegate {
                DataStorage.ClearDataPerDay();
            });
            callDialog.Show();
        }

    }
}