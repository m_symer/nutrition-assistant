using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    public class Calculator
    {
        //create texts of statistics
        public static string GetTextBilkoviny()
        {
            int celkem = DataStorage.dataPerDay.BilkovinyCelkem;
            int zaDen = DataStorage.dataPerDay.Bilkoviny;
            return zaDen.ToString() + " / " + celkem.ToString() + " g";
        }

        public static string GetTextSacharidy()
        {
            int celkem = DataStorage.dataPerDay.SacharidyCelkem;
            int zaDen = DataStorage.dataPerDay.Sacharidy;
            return zaDen.ToString() + " / " + celkem.ToString() + " g";
        }

        public static string GetProcentaBilkoviny()
        {
            float celkem = DataStorage.dataPerDay.BilkovinyCelkem;
            float zaDen = DataStorage.dataPerDay.Bilkoviny;
            float vypocet = (zaDen / celkem) * 100;
            if ((celkem == 0)||(zaDen == 0)) { vypocet = 0; }
            return vypocet > 100 ? "100%" : (vypocet.ToString("0") + "%");
        }

        public static string GetProcentaSacharidy()
        {
            float celkem = DataStorage.dataPerDay.SacharidyCelkem;
            float zaDen = DataStorage.dataPerDay.Sacharidy;
            float vypocet = (zaDen / celkem) * 100;
            if ((celkem == 0) || (zaDen == 0)) { vypocet = 0; }
            return vypocet > 100 ? "100%" : (vypocet.ToString("0") + "%");
        }

        public static int GetProgressBilkoviny()
        {
            float celkem = DataStorage.dataPerDay.BilkovinyCelkem;
            float zaDen = DataStorage.dataPerDay.Bilkoviny;
            float vypocet = (zaDen / celkem) * 100;
            if ((celkem == 0) || (zaDen == 0)) { vypocet = 0; }
            return (int)(vypocet > 100 ? 100 : vypocet);
        }

        public static int GetProgressSacharidy()
        {
            float celkem = DataStorage.dataPerDay.SacharidyCelkem;
            float zaDen = DataStorage.dataPerDay.Sacharidy;
            float vypocet = (zaDen / celkem) * 100;
            if ((celkem == 0) || (zaDen == 0)) { vypocet = 0; }
            return (int)(vypocet > 100 ? 100 : vypocet);
        }

    }
}