using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    [Activity(Label = "Meals")]
    public class EatActivity : Activity
    {
        ListView listView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Meals);

            listView = FindViewById<ListView>(Resource.Id.listViewMeals);
            
            listView.Adapter = new MealsAdapter(this, DataStorage.mealList);
            listView.ItemClick += ListView_ItemClick;

        }

        void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            DataStorage.UpdateDataPerDay(DataStorage.mealList[e.Position].Bilkoviny, DataStorage.mealList[e.Position].Sacharidy);
            Finish();
        }
    }
}