using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    [Activity(Label = "Meals")]
    public class MealsSettingsActivity : Activity
    {
        ListView listView;
        MealsAdapter adapter;

        protected override void OnResume()
        {
            base.OnResume();
            adapter.NotifyDataSetChanged();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.MealsSettings);

            listView = FindViewById<ListView>(Resource.Id.listViewJidlo);
            Button addMealButton = FindViewById<Button>(Resource.Id.buttonPridatJidlo);
            addMealButton.Click += AddMeal;
            
            adapter = new MealsAdapter(this, DataStorage.mealList);
            listView.Adapter = adapter;

            listView.ItemClick += ListView_ItemClick;
        }


        protected void AddMeal(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MealActivity));
            StartActivity(intent);
        }

        void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            RadioButton radiob = FindViewById<RadioButton>(Resource.Id.radioButtonOdstranovani);
            if (radiob.Checked)
            {
                DataStorage.DeleteItem(DataStorage.mealList[e.Position]._id);
                adapter.NotifyDataSetChanged();
            }
            else
            {
                var intent = new Intent(this, typeof(MealActivity));
                intent.PutExtra("MealToEdit", e.Position);
                StartActivity(intent);
            }
        }
    }
}