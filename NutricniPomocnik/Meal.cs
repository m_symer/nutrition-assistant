using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NutricniPomocnik
{
    public class Meal
    {
        public Guid _id { get; set; }
        public string Name { get; set;}
        public int Bilkoviny { get; set; }
        public int Sacharidy { get; set; }
    }
}