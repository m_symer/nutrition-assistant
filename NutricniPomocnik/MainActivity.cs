﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;

namespace NutricniPomocnik
{
    [Activity(Label = "Nutrition Assistant", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnResume()
        {
            base.OnResume();
            SetData();
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Button settingsButton = FindViewById<Button>(Resource.Id.buttonNastaveniAplikace);
            Button eatButton = FindViewById<Button>(Resource.Id.buttonJedlJsem);
            Button mealsButton = FindViewById<Button>(Resource.Id.buttonNastaveniJidel);

            settingsButton.Click += SettingsClick;
            eatButton.Click += EatClick;
            mealsButton.Click += JidlaClick;
            
            new DataStorage();
            new Calculator();

            DataStorage.GetDataPerDay();
        }

        protected void SettingsClick(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(SettingsActivity));
            StartActivity(intent);
        }

        protected void EatClick(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(EatActivity));
            StartActivity(intent);
        }

        protected void JidlaClick(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MealsSettingsActivity));
            StartActivity(intent);
        }

        public void SetData()
        {
            var procentaBilkoviny = FindViewById<TextView>(Resource.Id.textViewBilkovinyProcenta);
            var procentaSacharidy = FindViewById<TextView>(Resource.Id.textViewSacharidyProcenta);
            var pomerBilkoviny = FindViewById<TextView>(Resource.Id.textViewBilkovinyPomer);
            var pomerSacharidy = FindViewById<TextView>(Resource.Id.textViewSacharidyPomer);
            var progressBarBilkoviny = FindViewById<ProgressBar>(Resource.Id.progressBarBilkoviny);
            var progressBarSacharidy = FindViewById<ProgressBar>(Resource.Id.progressBarSacharidy);

            procentaBilkoviny.Text = Calculator.GetProcentaBilkoviny();
            procentaSacharidy.Text = Calculator.GetProcentaSacharidy();
            pomerSacharidy.Text = Calculator.GetTextSacharidy();
            pomerBilkoviny.Text = Calculator.GetTextBilkoviny();
            progressBarBilkoviny.Progress = Calculator.GetProgressBilkoviny();
            progressBarSacharidy.Progress = Calculator.GetProgressSacharidy();
        }

    }
}

